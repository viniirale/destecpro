package com.ufrgsapplication.destecpro;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    private static final String NAME = "name";
    SQLiteDatabase db;
    AutoCompleteTextView visitor_name;
    ArrayList<String> namesList;
    ArrayAdapter adapter;
    Boolean selectedClient = false;
    private Button b_questionario;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b_questionario = (Button) findViewById(R.id.button2);






        b_questionario.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,QuestionarioActivity.class));
            }
        });
    }
}
